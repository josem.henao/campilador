from PyQt5.QtWidgets import QFileDialog, QMessageBox
from PyQt5.uic.properties import QtGui

from compiler.compilador.compilador import Compilador


class CompilerWindowManager():
    __instancia = None

    def __new__(cls, **kwargs):
        if not cls.__instancia:
            cls.__instancia = object.__new__(cls)
            cls.main_window = kwargs.get('main_window')
            cls.text_editor_box = kwargs.get('text_editor')
            cls._actual_path = ''
        return cls.__instancia

    @property
    def main_window(self):
        return self._main_window

    @main_window.setter
    def main_window(self, main_window):
        self._main_window = main_window

    @property
    def text_editor_box(self):
        return self._text_editor_box

    @text_editor_box.setter
    def text_editor_box(self, text_editor_box):
        self._text_editor_box = text_editor_box

    @property
    def actual_path(self):
        return self._actual_path

    @actual_path.setter
    def actual_path(self, path):
        self.actual_path = path

    def get_instancia(self):
        return self.__instancia

    def open_file_name_dialog(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        file_path, _ = QFileDialog.getOpenFileName(options=options)
        self.write_on_editor(file_path)
        self.actual_path = file_path

    def write_on_editor(self, file_path):
        try:
            with open(file_path, 'r') as file:
                text = file.read()
                self.text_editor_box.setText(text)
        except IOError as err:
            print(f"Error: {err}")


    def save_as(self):
        saveFile = QtGui.QAction("&Save File", self)
        saveFile.setShortcut("Cmd+S")
        saveFile.setStatusTip('Guardar Archivo')
        saveFile.triggered.connect(self.file_save)

    def save_content(self):

        if self._confirm_save():
            try:
                print(f"Actual Path: {self.actual_path}")
                with open(self.actual_path, 'w') as file:
                    text = self.text_editor_box.toPlainText()
                    print(f"text: {text}")
                    file.write(text)
                    file.close()
                    msgBox = QMessageBox()
                    msgBox.setText("El documento ha sido Guardado correctamente")
                    msgBox.exec_()
            except IOError as err:
                print(f"Error: {err}")

    def compile(self):
        compilador = Compilador()

    def _confirm_save(self):
        buttonReply = QMessageBox.question(self.main_window, 'Confirmar Guardado',
                                           "Realmente quieres sobreescribir el archivo?",
                                           QMessageBox.Yes | QMessageBox.No | QMessageBox.Cancel, QMessageBox.Cancel)
        if buttonReply == QMessageBox.Yes:
            return True
        else:
            return False
