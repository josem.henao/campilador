from PyQt5.QtWidgets import QLabel


class Label(QLabel):
    def __init__(self, *args, text, resize, move, tooltip):
        super().__init__(*args)
        self.setText(text)
        self.resize(resize[0], resize[1])
        self.move(move[0], move[1])
        self.setToolTip(tooltip)
