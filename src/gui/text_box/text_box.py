from PyQt5.QtWidgets import QTextEdit


class TextBox(QTextEdit):

    def __init__(self, *args, text = None, resize = None , move = None, tooltip = None):
        super().__init__(*args)
        self.setText(text)
        self.resize(resize[0], resize[1])
        self.move(move[0], move[1])
        self.setToolTip(tooltip)