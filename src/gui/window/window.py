from PyQt5.QtWidgets import QMainWindow


class MainWindow(QMainWindow):
    def __init__(self, *args, title):
        super().__init__(*args)
        self.setGeometry(300,100,630,500)
        self.setWindowTitle(title)