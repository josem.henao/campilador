from PyQt5.QtWidgets import (
    QWidget,
    QPushButton,
    QMessageBox,
    QTextEdit,
    QFileDialog,
    QLineEdit,
    QApplication
)

import sys


class App(QWidget):

    def __init__(self):
        super().__init__()

        self.searchButton = QPushButton('Buscar', self)
        self.searchButton.resize(161, 31)
        self.searchButton.clicked.connect(self.openFileNameDialog)
        self.searchButton.move(630, 30)

        self.saveBtn = QPushButton('Cargar a editor', self)
        self.saveBtn.resize(131, 31)
        self.saveBtn.clicked.connect(self.writeOnEditor)
        self.saveBtn.move(480, 30)

        self.compileBtn = QPushButton('Compilar', self)
        self.compileBtn.resize(221, 51)
        self.compileBtn.clicked.connect(self.saveFile)
        self.compileBtn.move(310, 620)

        self.saveBtn = QPushButton('Guardar', self)
        self.saveBtn.resize(111, 31)
        self.saveBtn.clicked.connect(self.saveFile)
        self.saveBtn.move(640, 610)

        self.editorText = QTextEdit('', self)
        self.editorText.resize(691, 481)
        self.editorText.move(60, 120)

        self.routeText = QLineEdit('', self)
        self.routeText.resize(401, 31)
        self.routeText.move(50, 30)

        self.title = 'PyQt5 file dialogs - pythonspot.com'
        self.left = 10
        self.top = 10
        self.width = 825
        self.height = 697
        self.initUI()

    def initUI(self):

        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        # self.openFileNameDialog()
        # self.openFileNamesDialog()
        # self.saveFileDialog()

        self.show()

    def compileFile(self):
        print("El archivo está compilando")

    def openFileNameDialog(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getOpenFileName(self, "QFileDialog.getOpenFileName()", "",
                                                  "All Files (*);;Python Files (*.py)", options=options)

        if fileName:
            self.routeText.setText(fileName)
            self.writeOnEditor()

    def writeOnEditor(self):

        try:
            file = open(self.routeText.displayText(), 'r')

            with file:
                text = file.read()
                self.editorText.setText(text)

        except Exception as e:

            if self.routeText.displayText() == '':
                msgBox = QMessageBox()
                msgBox.setText("Error!!!, Primero debe seleccionar el archivo que desea cargar.")
                msgBox.exec_()
            else:
                msgBox = QMessageBox()
                msgBox.setText("Error!!!, La dirección ingresasada no existe.")
                msgBox.exec_()

    def openFileNamesDialog(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        files, _ = QFileDialog.getOpenFileNames(self, "QFileDialog.getOpenFileNames()", "",
                                                "All Files (*);;Python Files (*.py)", options=options)
        if files:
            print(files)

    def saveFile(self):
        # name, _ = QFileDialog.getSaveFileName(self,'Save File', options=QFileDialog.DontUseNativeDialog)
        try:
            file = open(self.routeText.displayText(), 'w')
            text = self.editorText.toPlainText()
            file.write(text)
            file.close()

            msgBox = QMessageBox()
            msgBox.setText("El documento ha sido modificado.")
            msgBox.exec_()

        except Exception as e:
            msgBox = QMessageBox()
            msgBox.setText("Error!!!, La dirección ingresasada no existe.")
            msgBox.exec_()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())