import sys

from PyQt5.QtWidgets import QApplication

from gui.button.push_button import PushButton
from gui.labels.path_label import Label
from gui.text_box.text_box import TextBox
from gui.window.window import MainWindow
from manager.managers import CompilerWindowManager

if __name__ == '__main__':
    app = QApplication(sys.argv)
    main_window = MainWindow(title="Compilador PYC")

    compiler_window_manager = CompilerWindowManager()
    compiler_window_manager = compiler_window_manager.get_instancia()

    compiler_window_manager.main_window = main_window

    path_label = Label(compiler_window_manager.main_window,
                       text = compiler_window_manager.actual_path,
                       resize=[60, 30],
                       move=[10, 10],
                       tooltip='Ruta del archivo actual'
                       )

    text_editor_box = TextBox(compiler_window_manager.main_window,
                              text='Ingresa Aquí su código',
                              resize=[600, 300],
                              move=[10, 70]
                              )
    compiler_window_manager.text_editor_box = text_editor_box

    open_button = PushButton(compiler_window_manager.main_window,
                             text='Abrir archivo',
                             resize=[100, 30],
                             move=[10, 30],
                             event_method = compiler_window_manager.open_file_name_dialog
                             )

    compile_button = PushButton(compiler_window_manager.main_window,
                             text='Compilar',
                             resize=[150, 50],
                             move=[250, 380],
                             event_method= compiler_window_manager.compile
                             )

    save_button = PushButton(compiler_window_manager.main_window,
                                text='Guardar',
                                resize=[100, 30],
                                move=[100, 30],
                                event_method=compiler_window_manager.save_as
                                )

    main_window.show()
    sys.exit(app.exec_())
