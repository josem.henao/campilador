from PyQt5.QtWidgets import QPushButton


class PushButton(QPushButton):

    def __init__(self, *args, **kwargs):
        super().__init__(*args)
        self.setText(kwargs.get('text'))
        self.resize(kwargs.get('resize')[0], kwargs.get('resize')[1])
        self.move(kwargs.get('move')[0], kwargs.get('move')[1])
        self.setToolTip(kwargs.get('tooltip'))
        self.clicked.connect(kwargs.get('event_method'))

