from compiler.dominio.archivo import Archivo
from compiler.dominio.linea import Linea


class Formateador:
    def __init__(self, file_path):
        self.file_path = file_path
        self._archivo = self._formatear_texto()

    @property
    def file_path(self):
        return self._file_path

    @file_path.setter
    def file_path(self, file_path):
        self._file_path = file_path

    @property
    def archivo(self):
        return self._archivo

    def _formatear_texto(self):
        try:
            archivo = Archivo().instancia
            with open(self.file_path) as file:
                lineas = file.readline().split('\n')
                archivo.agregar_lineas([Linea(i, lineas[i]) for i in range(len(lineas)) if lineas[i] != '' and lineas[i] != '\n'])
                # El mismo proceso anterior pero en la versión no Pythonic
                # for i in range(len(lineas)):
                #     if lineas[i] != '' and lineas[i] != '\n':
                #         archivo.agregar_linea(Linea(1, lineas[i]))
                archivo.agregar_EOF()
                return archivo
        except Exception as err:
            print(f'Error: {err}')
