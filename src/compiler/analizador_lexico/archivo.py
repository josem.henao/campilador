from abc import ABC

from compiler.analizador_lexico.linea import Linea

class Archivo(object, ABC):
    _instancia = None

    def __new__(cls, *args, **kwargs):
        if cls._instancia is None:
            cls._instancia = object.__new__(cls)
            cls.lineas = kwargs.get('lineas') or []
        return cls._instancia

    @property
    def lineas(self):
        return self._lineas

    @lineas.setter
    def lineas(self, lineas = []):
        self._lineas = lineas

    @property
    def instancia(self):
        return self._instancia

    def limpiar(self):
        self.lineas = []

    def agregar (self, contenido = ""):
        self.lineas.insert(
            len(self.lineas) - 1,
            Linea.crear(self._obtener_numero_proxima_linea(), contenido)
        )

    def obtener_linea(self, numero_linea):
        return filter(lambda x: x.numero == numero_linea, self.lineas) or None

    def _obtener_numero_proxima_linea(self):
        return len(self.lineas) + 1

    def _existe_linea(self, numero):
        return 0 <= numero - 1 < len(self.lineas)

    def agregar_lineas(self, lineas):
        for l in lineas:
            self.agregar_linea(l)

    # def obtener_contenido_linea(self, numero_linea):
    #     linea_retorno = None
    #     for linea in self.lineas:
    #         if linea.contenido == numero_linea:
    #             linea_retorno = linea
    #             break
    #     return linea_retorno
    #
    # def agregar_EOF(self):
    #     if len(self.lineas) == 0:
    #         self.lineas = [Linea(numero=0, contenido='@EOF@')]
    #     else:
    #         last_line = self.lineas[-1]
    #         if last_line.contenido != '@EOF@':
    #             self.lineas.append(Linea(numero = len(self.lineas) + 1, contenido='@EOF@'))
