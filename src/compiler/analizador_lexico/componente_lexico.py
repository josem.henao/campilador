from compiler.transversal.tipo_componente_lexico import TipoComponenteLexico


class ComponenteLexico():
    def __init__(self, *args, **kwargs):
        self.lexema = args[0] or kwargs.get('lexema')
        self.categoria = args[1] or kwargs.get('categoria')
        self.numero_linea = args[2] or kwargs.get('numero_linea')
        self.posicion_inicial = args[3] or kwargs.get('posicion_inicial')
        self.posicion_final = args[4] or kwargs.get('posicion_final')
        self.tipo = TipoComponenteLexico.COMPONENTE_LEXICO

    @staticmethod
    def crear(*args, **kwargs):
        return ComponenteLexico(*args, **kwargs)

    @property
    def lexema(self):
        return self._lexema

    @lexema.setter
    def lexema(self, lexema):
        self._lexema = lexema

    @property
    def categoria(self):
        return self._categoria

    @categoria.setter
    def categoria(self, categoria):
        self._categoria = categoria
        
    @property
    def numero_linea(self):
        return self._numero_linea
    
    @numero_linea.setter
    def numero_linea(self, numero):
        self._numero_linea = numero
        
    @property
    def posicion_inicial(self):
        return self._posicion_inicial

    @posicion_inicial.setter
    def posicion_inicial(self, posicion_inicial):
        self._posicion_inicial = posicion_inicial

    @property
    def posicion_final(self):
        return self._posicion_final

    @posicion_final.setter
    def posicion_final(self, posicion_final):
        self._posicion_final = posicion_final

    @property
    def tipo(self):
        return self._tipo

    @tipo.setter
    def tipo(self, tipo):
        self._tipo = tipo

