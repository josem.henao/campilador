from compiler.analizador_lexico.archivo import Archivo
from compiler.analizador_lexico.componente_lexico import ComponenteLexico
from compiler.analizador_lexico.linea import Linea
from compiler.transversal.Error import Error
from compiler.transversal.manejador_errores import ManejadorErrores


class AnalizadorLexico:
    def __init__(self):
        self._cargar_nueva_linea()

    @property
    def numero_linea_actual(self):
        return self._numero_linea_actual

    @numero_linea_actual.setter
    def numero_linea_actual(self, numero_linea_actual = 0):
        self._numero_linea_actual = numero_linea_actual

    @property
    def linea_actual(self):
        return self._linea_actual

    @linea_actual.setter
    def linea_actual(self, linea_actual : Linea):
        self._linea_actual = linea_actual

    @property
    def puntero(self):
        return self._puntero

    @puntero.setter
    def puntero(self, puntero:int):
        self._puntero = puntero

    @property
    def caracter_actual(self):
        return self._caracter_actual

    @caracter_actual.setter
    def caracter_actual(self, caracter_actual: str):
        self._caracter_actual = caracter_actual

    def _cargar_nueva_linea(self):
        self.numero_linea_actual += 1
        self.linea_actual = Archivo.instancia.obtener_linea(self._numero_linea_actual)

    def _leer_siguiente_caracter(self):
        if "@EOF@" == self.linea_actual.contenido:
            self.caracter_actual = self.linea_actual.contenido;
        elif self.puntero-1 >= len(self.linea_actual.contenido):
            self.caracter_actual = "@EOF@"
        else:
            self.caracter_actual = self.linea_actual.contenido[self.puntero-1:1]
            self.puntero += 1

    def _devolver_puntero(self):
            self.puntero -= 1

    def devolver_componente(self):
        """
        @:return ComponenteLexico()
        """
        componente = None
        estado_actual = 0
        continuar_analisis = True
        lexema = ""

        numeros = "1234567890"
        letras = "abcdefghijklmnopqrstuvwxyz"

        while(continuar_analisis):
            if estado_actual == 0:
                self._leer_siguiente_caracter()
                while " " == self.caracter_actual:
                    self._leer_siguiente_caracter()

                if self.caracter_actual in letras+"$_":
                    estado_actual = 4
                    lexema = lexema + self.caracter_actual

                elif self.caracter_actual in numeros:
                    estado_actual = 1
                    lexema = lexema + self.caracter_actual
                elif self.caracter_actual == '+':
                    estado_actual = 5
                    lexema = lexema + self.caracter_actual
                elif self.caracter_actual == '-':
                    estado_actual = 6
                    lexema = lexema + self.caracter_actual
                elif self.caracter_actual == '*':
                    estado_actual = 7
                    lexema = lexema + self.caracter_actual
                elif self.caracter_actual == '/':
                    estado_actual = 8
                    lexema = lexema + self.caracter_actual
                elif self.caracter_actual == '%':
                    estado_actual = 9
                    lexema = lexema + self.caracter_actual
                elif self.caracter_actual == '(':
                    estado_actual = 10
                    lexema = lexema + self.caracter_actual
                elif self.caracter_actual == ')':
                    estado_actual = 11
                    lexema = lexema + self.caracter_actual
                elif self.caracter_actual == '@EOF@':
                    estado_actual = 12
                    lexema = lexema + self.caracter_actual
                elif self.caracter_actual == '=':
                    estado_actual = 19
                    lexema = lexema + self.caracter_actual
                elif self.caracter_actual == '<':
                    estado_actual = 20
                    lexema = lexema + self.caracter_actual
                elif self.caracter_actual == '>':
                    estado_actual = 21
                    lexema = lexema + self.caracter_actual
                elif self.caracter_actual == ':':
                    estado_actual = 22
                    lexema = lexema + self.caracter_actual
                elif self.caracter_actual == '!':
                    estado_actual = 30
                    lexema = lexema + self.caracter_actual
                elif self.caracter_actual == '@FL@':
                    estado_actual = 13
                    lexema = lexema + self.caracter_actual
                else:
                    estado_actual = 18
                    lexema = lexema + self.caracter_actual


            elif estado_actual == 1:
                self._leer_siguiente_caracter()
                if self.caracter_actual in numeros:
                    estado_actual = 1
                    lexema = lexema + self.caracter_actual
                elif self.caracter_actual == '.':
                    estado_actual = 2
                    lexema = lexema + self.caracter_actual
                else:
                    estado_actual = 14
                    lexema = lexema + self.caracter_actual

            elif estado_actual == 2:
                self._leer_siguiente_caracter()
                if self.caracter_actual in numeros:
                    estado_actual = 3
                    lexema = lexema + self.caracter_actual
                else:
                    estado_actual = 15
                    lexema = lexema + self.caracter_actual

            elif estado_actual == 4:
                self._leer_siguiente_caracter()
                if self.caracter_actual in numeros+letras+"$_":
                    estado_actual = 4
                    lexema = lexema + self.caracter_actual
                else:
                    estado_actual = 16
                    lexema = lexema + self.caracter_actual

            elif estado_actual == 5:
                continuar_analisis = False
                componente = ComponenteLexico.crear(lexema = lexema,
                                                    categoria = 'SUMA',
                                                    numero_linea = self.linea_actual.numero,
                                                    posicion_inicial = self.puntero - len(lexema),
                                                    posicion_final = self.puntero - 1
                                                    )
            elif estado_actual == 6:
                continuar_analisis = False
                componente = ComponenteLexico.crear(lexema = lexema,
                                                    categoria = 'RESTA',
                                                    numero_linea = self.linea_actual.numero,
                                                    posicion_inicial = self.puntero - len(lexema),
                                                    posicion_final = self.puntero - 1
                                                    )

            elif estado_actual == 7:
                continuar_analisis = False
                componente = ComponenteLexico.crear(lexema = lexema,
                                                    categoria = 'MULTIPLICACION',
                                                    numero_linea = self.linea_actual.numero,
                                                    posicion_inicial = self.puntero - len(lexema),
                                                    posicion_final = self.puntero - 1
                                                    )

            elif estado_actual == 8:
                self._leer_siguiente_caracter()
                if self.caracter_actual == "/":
                    estado_actual = 36
                    lexema = lexema + self.caracter_actual
                elif self.caracter_actual == "*":
                    estado_actual = 34
                    lexema = lexema + self.caracter_actual
                else:
                    estado_actual = 33
                    lexema = lexema + self.caracter_actual

            elif estado_actual == 9:
                continuar_analisis = False
                componente = ComponenteLexico.crear(lexema = lexema,
                                                    categoria = 'MODULO',
                                                    numero_linea = self.linea_actual.numero,
                                                    posicion_inicial =  self.puntero - len(lexema),
                                                    posicion_final = self.puntero - 1
                                                    )
            elif estado_actual == 10:
                continuar_analisis = False
                componente = ComponenteLexico.crear(lexema=lexema,
                                                    categoria='PARENTESIS_ABRE',
                                                    numero_linea=self.linea_actual.numero,
                                                    posicion_inicial=self.puntero - len(lexema),
                                                    posicion_final=self.puntero - 1
                                                    )
            elif estado_actual == 11:
                continuar_analisis = False
                componente = ComponenteLexico.crear(lexema=lexema,
                                                    categoria='PARENTESIS_CIERRA',
                                                    numero_linea=self.linea_actual.numero,
                                                    posicion_inicial=self.puntero - len(lexema),
                                                    posicion_final=self.puntero - 1
                                                    )
            elif estado_actual == 12:
                continuar_analisis = False
                componente = ComponenteLexico.crear(lexema=lexema,
                                                    categoria='FIN_ARCHIVO',
                                                    numero_linea=self.linea_actual.numero,
                                                    posicion_inicial=self.puntero - len(lexema),
                                                    posicion_final=self.puntero - 1
                                                    )
            elif estado_actual == 13:
                self._cargar_nueva_linea()
                self.puntero = 0
                estado_actual = 0

            elif estado_actual == 14:
                continuar_analisis = False
                componente = ComponenteLexico.crear(lexema=lexema,
                                                    categoria='NUMERO_ENTERO',
                                                    numero_linea=self.linea_actual.numero,
                                                    posicion_inicial=self.puntero - len(lexema),
                                                    posicion_final=self.puntero - 1
                                                    )
                self._devolver_puntero()

            elif estado_actual == 15:
                continuar_analisis = False
                componente = ComponenteLexico.crear(lexema=lexema,
                                                    categoria='NUMERO_DECIMAL',
                                                    numero_linea=self.linea_actual.numero,
                                                    posicion_inicial=self.puntero - len(lexema),
                                                    posicion_final=self.puntero - 1
                                                    )
                self._devolver_puntero()

            elif estado_actual == 16:
                self._devolver_puntero()
                continuar_analisis = False
                componente = ComponenteLexico.crear(lexema=lexema,
                                                    categoria='IDENTIFICADOR',
                                                    numero_linea=self.linea_actual.numero,
                                                    posicion_inicial=self.puntero - len(lexema),
                                                    posicion_final=self.puntero - 1
                                                    )

            elif estado_actual == 17:
                self._devolver_puntero()
                continuar_analisis = False
                ManejadorErrores().instancia.agregar(Error.crear_error_lexico(
                        lexema,
                        "Se esperaba un número y se recibio " + self.caracter_actual,
                        "Asegurarse de que el número decimal sea válido",
                        self.linea_actual.numero,
                        self.puntero - len(lexema),
                        self.puntero - 1
                    ))


            elif estado_actual == 18:
                continuar_analisis = False
                # Debe retornar una Error "Numero decimal no válido"
                # self._devolver_puntero()
                pass

            elif estado_actual == 19:
                continuar_analisis = False
                componente = ComponenteLexico.crear(lexema=lexema,
                                                    categoria='IGUAL_QUE',
                                                    numero_linea=self.linea_actual.numero,
                                                    posicion_inicial=self.puntero - len(lexema),
                                                    posicion_final=self.puntero - 1
                                                    )
            elif estado_actual == 20:
                self._leer_siguiente_caracter()
                if self.caracter_actual == ">":
                    estado_actual = 23
                    lexema = lexema + self.caracter_actual
                elif self.caracter_actual == "=":
                    estado_actual = 24
                    lexema = lexema + self.caracter_actual
                else:
                    continuar_analisis = False
                    componente = ComponenteLexico.crear(lexema=lexema,
                                                        categoria='MENOR_QUE',
                                                        numero_linea=self.linea_actual.numero,
                                                        posicion_inicial=self.puntero - len(lexema),
                                                        posicion_final=self.puntero - 1
                                                        )
                    self._devolver_puntero()

            elif estado_actual == 21:
                self._leer_siguiente_caracter()
                if self.caracter_actual == "=":
                    estado_actual = 26
                    lexema = lexema + self.caracter_actual
                else:
                    continuar_analisis = False
                    componente = ComponenteLexico.crear(lexema=lexema,
                                                        categoria='MAYOR_QUE',
                                                        numero_linea=self.linea_actual.numero,
                                                        posicion_inicial=self.puntero - len(lexema),
                                                        posicion_final=self.puntero - 1
                                                        )
                    self._devolver_puntero()


            elif estado_actual == 22:
                self._leer_siguiente_caracter()
                if self.caracter_actual == "=":
                    estado_actual = 28
                    lexema = lexema + self.caracter_actual
                else:
                    estado_actual = 29
                    lexema = lexema + self.caracter_actual

            elif estado_actual == 23:
                continuar_analisis = False
                componente = ComponenteLexico.crear(lexema=lexema,
                                                    categoria='DIFERENTE_QUE',
                                                    numero_linea=self.linea_actual.numero,
                                                    posicion_inicial=self.puntero - len(lexema),
                                                    posicion_final=self.puntero - 1
                                                    )
            elif estado_actual == 24:
                continuar_analisis = False
                componente = ComponenteLexico.crear(lexema=lexema,
                                                    categoria='MENOR_IGUAL_QUE',
                                                    numero_linea=self.linea_actual.numero,
                                                    posicion_inicial=self.puntero - len(lexema),
                                                    posicion_final=self.puntero - 1
                                                    )
            elif estado_actual == 25:
                continuar_analisis = False
                componente = ComponenteLexico.crear(lexema=lexema,
                                                    categoria='MENOR_QUE',
                                                    numero_linea=self.linea_actual.numero,
                                                    posicion_inicial=self.puntero - len(lexema),
                                                    posicion_final=self.puntero - 1
                                                    )
                self._devolver_puntero()

            elif estado_actual == 26:
                continuar_analisis = False
                componente = ComponenteLexico.crear(lexema=lexema,
                                                    categoria='MAYOR_IGUAL_QUE',
                                                    numero_linea=self.linea_actual.numero,
                                                    posicion_inicial=self.puntero - len(lexema),
                                                    posicion_final=self.puntero - 1
                                                    )
            elif estado_actual == 27:
                continuar_analisis = False
                componente = ComponenteLexico.crear(lexema=lexema,
                                                    categoria='MAYOR_QUE',
                                                    numero_linea=self.linea_actual.numero,
                                                    posicion_inicial=self.puntero - len(lexema),
                                                    posicion_final=self.puntero - 1
                                                    )
            elif estado_actual == 28:
                continuar_analisis = False
                componente = ComponenteLexico.crear(lexema=lexema,
                                                    categoria='ASIGNACION',
                                                    numero_linea=self.linea_actual.numero,
                                                    posicion_inicial=self.puntero - len(lexema),
                                                    posicion_final=self.puntero - 1
                                                    )

            elif estado_actual == 29:
                continuar_analisis = False
                # Debe retornar una Error "Asignación no válida"
                # self._devolver_puntero()
                pass

            elif estado_actual == 30:
                self._leer_siguiente_caracter()
                if self.caracter_actual == "=":
                    estado_actual = 31
                    lexema = lexema + self.caracter_actual
                else:
                    #Error "Error de asignación no válida"
                    pass

            elif estado_actual == 31:
                continuar_analisis = False
                componente = ComponenteLexico.crear(lexema=lexema,
                                                    categoria='DIFERENTE_QUE',
                                                    numero_linea=self.linea_actual.numero,
                                                    posicion_inicial=self.puntero - len(lexema),
                                                    posicion_final=self.puntero - 1
                                                    )
            elif estado_actual == 32:
                continuar_analisis = False
                # Debe retornar una Error "Asignación no válida"
                # self._devolver_puntero()
                pass

            elif estado_actual == 33:
                continuar_analisis = False
                componente = ComponenteLexico.crear(lexema=lexema,
                                                    categoria='DIVISION',
                                                    numero_linea=self.linea_actual.numero,
                                                    posicion_inicial=self.puntero - len(lexema),
                                                    posicion_final=self.puntero - 1
                                                    )
            elif estado_actual == 34:
                if self.caracter_actual == "@FL@":
                    estado_actual = 31
                    lexema = lexema + self.caracter_actual
                elif self.caracter_actual == "*":
                    estado_actual = 31
                    lexema = lexema + self.caracter_actual
                else:
                    estado_actual = 34
                    lexema = lexema + self.caracter_actual

            elif estado_actual == 35:
                if self.caracter_actual == "/":
                    estado_actual = 0
                    lexema = lexema + self.caracter_actual
                elif self.caracter_actual == "*":
                    estado_actual = 35
                    lexema = lexema + self.caracter_actual
                else:
                    estado_actual = 34
                    lexema = lexema + self.caracter_actual

            elif estado_actual == 36:
                if self.caracter_actual == "@FL@":
                    estado_actual = 13
                    lexema = lexema + self.caracter_actual
                else:
                    estado_actual = 36
                    lexema = lexema + self.caracter_actual

            elif estado_actual == 37:
                self._cargar_nueva_linea()

        return componente