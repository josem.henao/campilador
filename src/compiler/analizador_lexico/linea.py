class Linea:
    def __init__(self, args, kwargs) -> object:
        self.numero = args[0] or kwargs.get('numero')
        self.contenido = args[1] or kwargs.get('contenido')

    @property
    def numero(self):
        return self._numero

    @numero.setter
    def numero(self, numero):
        self._numero = numero

    @property
    def contenido(self):
        return self._contenido

    @contenido.setter
    def contenido(self, contenido):
        self._contenido = contenido

    @staticmethod
    def crear(numero: int, contenido: str):
        return Linea(numero, contenido)
