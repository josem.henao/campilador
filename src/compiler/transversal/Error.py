from compiler.transversal.tipo_error import TipoError


class Error():
    def __init__(self, *args, **kwargs):
        self.tipo = args[0] or kwargs.get('tipo')
        self.lexema = args[1] or kwargs.get('lexema')
        self.causa = args[2] or kwargs.get('causa')
        self.falla = args[3] or kwargs.get('falla')
        self.solucion = args[4] or kwargs.get('solucion')
        self.numero_linea = args[5] or kwargs.get('numero_linea')
        self.posicion_inicial = args[6] or kwargs.get('posicion_inicial')
        self.posicion_final = args[6] or kwargs.get('posicion_final')

    @staticmethod
    def crear_error_lexico(*args, **kwargs):
        return Error(*args, **kwargs, tipo = TipoError.LEXICO)

    @staticmethod
    def crear_error_sintactico(*args, **kwargs):
        return Error(*args, **kwargs, tipo = TipoError.SINTACTICO)

    @staticmethod
    def crear_error_semantico(*args, **kwargs):
        return Error(*args, **kwargs, tipo = TipoError.SEMANTICO)

    @property
    def tipo(self):
        return self._tipo_error

    @property
    def lexema(self):
        return self._lexema

    @property
    def causa(self):
        return self._causa

    @property
    def falla(self):
        return self._falla

    @property
    def solucion(self):
        return self._solucion

    @property
    def numero_linea(self):
        return self._numero_linea

    @property
    def posicion_inicial(self):
        return self._posicion_inicial

    @property
    def posicion_final(self):
        return self._posicion_final
