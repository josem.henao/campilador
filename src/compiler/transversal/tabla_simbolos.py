class TablaSimbolos:
    _instancia = None

    def __init__(self):
        pass

    def __new__(cls, *args, **kwargs):
        if cls.instancia is None:
            cls._instancia = object.__new__(cls)

        return cls._instancia

    @property
    def instancia(self):
        return self._instancia

    @property
    def tabla(self):
        return self._tabla

    @tabla.setter
    def tabla(self, tabla):
        self._tabla = tabla

    def obtener_instancia(self):
        return self.instancia

    def obtener_simbolo(self, lexema):
        if lexema not in self.tabla:
            self.tabla[lexema] = []
        return self.tabla[lexema]