from enum import Enum

class TipoError(Enum):
    LEXICO = 1
    SINTACTICO = 2
    SEMANTICO = 3
