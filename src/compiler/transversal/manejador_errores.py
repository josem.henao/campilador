from compiler.transversal.Error import Error


class ManejadorErrores:
    _instancia = None
    _mapa_errores = {
        "LEXICO": [],
        "SINTACTICO": [],
        "SEMANTICO": []

    }

    def __new__(cls, *args, **kwargs):
        if cls._instancia is None:
            cls._instancia = object.__new__(cls)
        return cls._instancia

    @property
    def instancia(self):
        return self._instancia

    @property
    def mapa_errores(self):
        return self._mapa_errores

    def agregar(self, error: Error):
        self.mapa_errores[error.tipo].append(error)

    def limpiar(self, tipo_error):
        self.mapa_errores.pop(tipo_error, None)

    def obtener_errores(self, tipo_error):
        return self.mapa_errores.get(tipo_error)

    def obtener_todos_errores(self):
        errores = []
        for k in self.mapa_errores.keys():
            for e in self.mapa_errores.get(k):
                errores.append(e.value)
        return errores
